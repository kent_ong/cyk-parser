<?php
if(isset($_POST['sent']))
{
	$sent = true;
	
	try
	{
		if($_FILES["grammar-file"]["type"] == "text/plain")
		{
			if ($_FILES["grammar-file"]["error"] > 0)
			{
				throw new Exception("Erro no envio do arquivo. Código: {$_FILES["grammar-file"]["error"] }");
			}
			else
			{
				if (file_exists("grammars/" . $_FILES["grammar-file"]["name"]))
				{
					throw new Exception("O arquivo {$_FILES["grammar-file"]["name"]} já existe.");
				}
				else
				{
					move_uploaded_file($_FILES["grammar-file"]["tmp_name"], "grammars/" . $_FILES["grammar-file"]["name"]);
				}
			}
		}
		else
		{
			throw new Exception("Arquivo inválido.");
		}
	}
	catch(Exception $e)
	{
		$error = $e->getMessage();	
	}

}
?>


<?php include("public/header.php"); ?>

<?php if(!isset($error) && isset($sent)):?>
<div class="grid_24">
	<div class="success-message">
		O Arquivo foi enviado com sucesso.
	</div>
</div>
<?php elseif(isset($error) && isset($sent)): ?>
<div class="grid_24">
	<div class="error-message">
		<?php echo $error ?>
	</div>
</div>
<?php endif;?>

<form action="upload.php" method="post" enctype="multipart/form-data">
	<input type="hidden" name="sent" value="true" />
	<div class="grid_24">
	<fieldset>
		<dl>
			<dt>
				<label class="">Arquivo da Gramática:</label>
			</dt>
			<dd>
				<div class=""><input type="file" name="grammar-file" /></div>
			</dd>
		</dl>
		
		<dl>
			<dd>
				<button type="submit">Enviar</button>
			</dd>
		</dl>
	</fieldset>
	</div>
</form>

<?php include("public/footer.php") ?>