<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="pt-BR" xmlns:og="http://ogp.me/ns#">
<head profile="http://gmpg.org/xfn/11">
    <title>CYK-Parser - INF/UFRGS/2012</title>
 
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    
    <link href='http://fonts.googleapis.com/css?family=Black+Ops+One' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="public/css/style.css" />
	<link type="text/css" href="public/css/Spacetree.css" rel="stylesheet" />
	
	<!--[if IE]><script language="javascript" type="text/javascript" src="excanvas.js"></script><![endif]-->
	<script language="javascript" type="text/javascript" src="public/js/jit-yc.js"></script>
     
</head>
<body>
<div id="header" class="clearfix">
	<a href="index.php">CYK-Parser</a>
</div>

<div id="wrapper" class="container_24 hfeed">
    <div id="main">
<div id="infovis"></div>   